/*
 * @Author: Raiz
 * @since: Do not edit
 * @lastTime: 2020-08-07 09:35:37
 * @LastEditors: Raiz
 * @Description: 
 */
import request from '@/utils/request'

const baseUrl = '/billType/'

export function queryBillTypeTree(data) {
  return request({
    url: baseUrl + 'queryBillTypeTree',
    method: 'post',
    params: data
  })
}