/*
 * @Author: yyd
 * @since: 2019-05-19 10:10:06
 * @lastTime: 2020-08-07 09:34:49
 * @LastEditors: Raiz
 * @Description: 
 */ 
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(Element)
Vue.config.productionTip = false
/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})
