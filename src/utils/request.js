/*
 * @Author: Raiz
 * @since: 2020-07-07 14:02:48
 * @lastTime: 2020-08-06 17:25:37
 * @LastEditors: Raiz
 * @Description:
 */
import axios from 'axios'
import { Message } from 'element-ui'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  config => {
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

function errorLog(error) {
  // 显示提示
  Message({
    message: error.message,
    type: 'error',
    duration: 5 * 1000
  })
}
// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response
    const status = res.status
    // if the custom code is not 20000, it is judged as an error.
    if (status === 200 ) {
      return res.data
    }else {
      Message({
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000
      })
    }
  },
  error => {
    if (error && error.response) {
      switch (error.response.status) {
        case 400: error.message = '请求错误'; errorLog(error); break
        case 401: error.message = '登陆失效请重新登陆'; errorLog(error); break
        case 403: error.message = '拒绝访问'; errorLog(error); break
        case 404: error.message = `请求地址出错: ${error.response.config.url}`; errorLog(error); break
        case 408: error.message = '请求超时'; errorLog(error); break
        case 500: error.message = '服务器 内部错误'; errorLog(error); break
        case 501: error.message = '服务未实现'; errorLog(error); break
        case 502: error.message = '网络错误'; errorLog(error); break
        case 503: error.message = '服务不可用'; errorLog(error); break
        case 504: error.message = '网络超时'; errorLog(error); break
        case 505: error.message = 'HTTP版本不受支持'; errorLog(error); break
        default: break
      }
    }
  }
)

export default service
